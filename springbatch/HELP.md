### SQL

CREATE TABLE public.wiki_doc
(
    id serial  NOT NULL,
    url varchar(300) NOT NULL,
    title varchar(300) NOT NULL,
    CONSTRAINT "PK_ID" PRIMARY KEY (id)
);

ALTER TABLE public.wiki_doc
    OWNER to postgres;

### Source XML File 

https://ftp.acc.umu.se/mirror/wikimedia.org/dumps/enwiki/20210320/enwiki-20210320-abstract3.xml.gz
https://ftp.acc.umu.se/mirror/wikimedia.org/dumps/enwiki/20210320/enwiki-20210320-abstract1.xml.gz


### application.properties

``` spring.batch.initialize-schema=always ```

this step is necessary if we are using external data source.









