package com.ionixx.springbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbatchBechmarkApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchBechmarkApplication.class, args);
	}

}
