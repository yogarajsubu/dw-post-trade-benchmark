package com.ionixx.springbatch.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import com.ionixx.springbatch.domain.Doc;
import com.thoughtworks.xstream.XStream;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.batch.item.xml.builder.StaxEventItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.oxm.xstream.XStreamMarshaller;

//https://medium.com/swlh/working-with-spring-batch-and-distributed-transaction-772de2219e60#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc3NDU3MzIxOGM2ZjZhMmZlNTBlMjlhY2JjNjg2NDMyODYzZmM5YzMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJuYmYiOjE2MTg1NzcwMDMsImF1ZCI6IjIxNjI5NjAzNTgzNC1rMWs2cWUwNjBzMnRwMmEyamFtNGxqZGNtczAwc3R0Zy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwNDgxNjkwNzI3NDY1NjIyNDg3NyIsImhkIjoiaW9uaXh4dGVjaC5jb20iLCJlbWFpbCI6InlvZ2FyYWouc3VicmFtYW5pYW5AaW9uaXh4dGVjaC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMjE2Mjk2MDM1ODM0LWsxazZxZTA2MHMydHAyYTJqYW00bGpkY21zMDBzdHRnLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwibmFtZSI6IllvZ2FyYWogU3VicmFtYW5pYW4iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FPaDE0R2huNzE4RE5NZ3FOZU4zTk9JZkFjazJSc1dPRThRQ0oydW1ma0NqOVE9czk2LWMiLCJnaXZlbl9uYW1lIjoiWW9nYXJhaiIsImZhbWlseV9uYW1lIjoiU3VicmFtYW5pYW4iLCJpYXQiOjE2MTg1NzczMDMsImV4cCI6MTYxODU4MDkwMywianRpIjoiOTc4OTY2ZTEwOTk1NjcyN2IyNjRjYzlhM2FlODZiNGNjYzRjMTFjOCJ9.KSBtSxrBLcu_eaqv7j4nJ3VQNFsjHzKbXZ4soJyHQ9fAz5obonWXFlLtJS2bF2k0vt48ksuieorMsYQH9SVNTxkNMGITBrcNTiQ9MzPWyr6ZweHT8cdoUJ9TTNhZ9OZm8SUok3yrFI_BLsM3NVjkWUqJkiaHIIC3JcwnegXkZK78eCXN-QQOwtTAjipApaN0Ch9gNxE1KcZcYFcEIrhS2w3dqlkh8qTnXx2s7wVcY__bzmpujkZBaE9I7iyYH1ojFIUIGiAsWClS-vnJo8heBTINtHfd5lyDLF5FljnZNRUyN-cCVy8A-W8otlnYLMl6qNox_LkaZA7l-IVqqatmMg
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    JobBuilderFactory mJobBuilderFactory;
    

    @Autowired
    StepBuilderFactory mStepBuilderFactory;

    @Bean
    public XStreamMarshaller docMarshaller() {
        Map<String, Class> aliases = new HashMap<>();
        aliases.put("doc", Doc.class);
        aliases.put("title", String.class);
        aliases.put("url", String.class);

        XStreamMarshaller marshaller = new XStreamMarshaller() {
            @Override
            protected void configureXStream(XStream xstream) {
                super.configureXStream(xstream);
                xstream.ignoreUnknownElements();
            }
        };

        marshaller.setAliases(aliases);

        return marshaller;
    }

    @Bean
    public StaxEventItemReader<Doc> itemReader() {
        return new StaxEventItemReaderBuilder<Doc>().name("docItemReader")
                .resource(new FileSystemResource("/Users/yogaraj/Desktop/enwiki-20210520-abstract3.xml"))
                .addFragmentRootElements("doc").unmarshaller(docMarshaller()).build();
    }

    @Bean
    public JdbcBatchItemWriter<Doc> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Doc>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO wiki_doc ( title,url ) VALUES (:title, :url)").dataSource(dataSource).build();
    }

    @Bean
    public Step step(JdbcBatchItemWriter<Doc> writer) {
        return mStepBuilderFactory.get("xmlReader").<Doc, Doc>chunk(1000).reader(itemReader()).writer(writer).build();
    }

    @Bean
    public Job importWikiDocJob(Step step) {
        return mJobBuilderFactory.get("importWikiJob").flow(step).end().preventRestart().build();
    }

}
