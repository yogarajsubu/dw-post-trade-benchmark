package com.ionixx.springbatch;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbatchBechmarkApplicationTests {

	@Autowired
	Job docJob;

	@Autowired
	JobLauncher docJobLauncher;

	@Test
	void jobExecutionTest() throws Exception {
		// need to make sure the job param is different
		JobParameters jobParameters = new JobParametersBuilder().addDate("id", new Date())
				.addLong("run.id", new Date().getTime()).toJobParameters();

		docJobLauncher.run(docJob, jobParameters);
	}

}
