package com.ionixx.schemagenerator.dto;

import com.ionixx.schemagenerator.GraphQLField;
import com.ionixx.schemagenerator.GraphQLSchema;
import com.ionixx.schemagenerator.GraphQLField.FieldType;
import com.ionixx.schemagenerator.GraphQLSchema.SchemaType;

@GraphQLSchema(operationName = "docType", schemaType = SchemaType.ENUM)
public enum DocType {
    @GraphQLField(fieldName = "media", fieldType = FieldType.STRING)
    MEDIA, @GraphQLField(fieldName = "document", fieldType = FieldType.STRING)
    DOCUMENT
}
