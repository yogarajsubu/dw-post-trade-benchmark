package com.ionixx.schemagenerator.dto;

import java.util.List;

import com.fasterxml.jackson.module.kotlin.ReflectionCache.BooleanTriState.True;
import com.ionixx.schemagenerator.GraphQLField;
import com.ionixx.schemagenerator.GraphQLSchema;
import com.ionixx.schemagenerator.GraphQLField.FieldType;
import com.ionixx.schemagenerator.GraphQLSchema.SchemaType;

@GraphQLSchema(schemaType = SchemaType.QUERY, operationName = "docQuery")
public class DocQuery {

    @GraphQLField(fieldType = FieldType.LIST, returnType = Doc.class, isNullable = true)
    public List<Doc> getAllDoc;
}
