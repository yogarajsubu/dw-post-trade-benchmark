package com.ionixx.schemagenerator.dto;

import com.ionixx.schemagenerator.GraphQLField;
import com.ionixx.schemagenerator.GraphQLSchema;
import com.ionixx.schemagenerator.GraphQLSchema.SchemaType;


@GraphQLSchema(schemaType = SchemaType.OBJECT, operationName = "doc")
public class Doc {

    @GraphQLField
    public String title;

    @GraphQLField
    public String url;

}
