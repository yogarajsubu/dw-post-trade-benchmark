package com.ionixx.schemagenerator.resolver;

import java.util.ArrayList;
import java.util.List;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.ionixx.schemagenerator.dto.Doc;

import org.springframework.stereotype.Component;

@Component
public class DocQueryResolver implements GraphQLQueryResolver {
    public List<Doc> getAllDoc() {
        Doc doc = new Doc();
        doc.url = "https://testurl.com";
        doc.title = "test data";
        List<Doc> dList = new ArrayList<>();
        dList.add(doc);
        return dList;
    }
}
