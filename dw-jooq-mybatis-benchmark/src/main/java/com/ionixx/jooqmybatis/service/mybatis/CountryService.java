package com.ionixx.jooqmybatis.service.mybatis;
import com.dw.utility.exceptions.SQLException;
import java.util.List;

import com.yogaraj.posttrade.tables.pojos.City;
import com.yogaraj.posttrade.tables.pojos.Country;
import com.yogaraj.posttrade.tables.pojos.WikiDoc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("MybatisCountryService")
public class CountryService {

    @Autowired
    CountryMapper mapper;

    public List<Country> getCountryList() {
        return mapper.fetchAllCountry();
    }

    public List<City> getCityList() {
        return mapper.fetchAllCity();
    }

    public List<Object> getCountryWithCities() {
        return mapper.fetchCountriesWithCities();
    }

    public List<WikiDoc> getAllDocs() {
        return mapper.getAllDocs();
    }
}
