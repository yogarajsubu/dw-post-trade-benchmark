package com.ionixx.jooqmybatis.service.mybatis;
import com.dw.utility.exceptions.SQLException;
import java.util.List;

import com.yogaraj.posttrade.tables.pojos.City;
import com.yogaraj.posttrade.tables.pojos.Country;
import com.yogaraj.posttrade.tables.pojos.WikiDoc;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CountryMapper {

    @Select("select * from country")
    List<Country> fetchAllCountry();

    @Select("select * from city")
    List<City> fetchAllCity();

    @Select("select * from city left join country c on c.code = city.countrycode")
    List<Object> fetchCountriesWithCities();

    @Select("select * from wiki_doc")
    List<WikiDoc> getAllDocs();
}
