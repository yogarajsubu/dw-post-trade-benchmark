package com.ionixx.jooqmybatis.service.jooq;
import com.dw.utility.exceptions.SQLException;
import java.util.List;

import com.yogaraj.posttrade.Tables;
import com.yogaraj.posttrade.tables.pojos.City;
import com.yogaraj.posttrade.tables.pojos.Country;
import com.yogaraj.posttrade.tables.pojos.WikiDoc;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JooqCountryService {

    @Autowired
    DSLContext mDslContext;

    public List<Country> getCountryList() {
        return mDslContext.selectFrom(Tables.COUNTRY).fetchInto(Country.class);
    }

    public List<City> getCityList() {
        return mDslContext.selectFrom(Tables.CITY).fetchInto(City.class);
    }

    public Object[] getCountryWithCities() {

        return mDslContext.select().from(Tables.CITY).join(Tables.COUNTRY)
                .on(Tables.CITY.COUNTRYCODE.eq(Tables.COUNTRY.CODE)).fetchArray();
    }

    public List<WikiDoc> getALWikiDocs() {
        return mDslContext.select().from(Tables.WIKI_DOC).fetchInto(WikiDoc.class);
    }
}
