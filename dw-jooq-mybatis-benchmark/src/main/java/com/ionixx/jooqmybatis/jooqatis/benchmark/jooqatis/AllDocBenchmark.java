package com.ionixx.jooqmybatis.jooqatis.benchmark.jooqatis;

import java.util.concurrent.ExecutorService;

import com.ionixx.jooqmybatis.service.jooq.JooqCountryService;
import com.ionixx.jooqmybatis.service.mybatis.CountryService;

import org.springframework.stereotype.Service;

import io.micrometer.core.instrument.MeterRegistry;

@Service
public class AllDocBenchmark extends AbstractBenchmark {

    public AllDocBenchmark(MeterRegistry meterRegistry, JooqCountryService jooqCountryService,
            CountryService myBatisCountryService, ExecutorService executorService) {

        super(() -> jooqCountryService.getALWikiDocs().size(), () -> myBatisCountryService.getAllDocs().size(),
                meterRegistry, executorService);
    }

}
