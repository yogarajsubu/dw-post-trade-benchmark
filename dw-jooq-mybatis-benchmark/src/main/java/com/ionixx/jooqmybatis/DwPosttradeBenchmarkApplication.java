package com.ionixx.jooqmybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DwPosttradeBenchmarkApplication {

	public static void main(String[] args) {
		SpringApplication.run(DwPosttradeBenchmarkApplication.class, args);
	}

}
