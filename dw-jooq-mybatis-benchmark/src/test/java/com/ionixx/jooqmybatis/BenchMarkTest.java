package com.ionixx.jooqmybatis;

import java.util.logging.Logger;

import com.ionixx.jooqmybatis.service.jooq.JooqCountryService;
import com.ionixx.jooqmybatis.service.mybatis.CountryService;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

// functional test
@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class BenchMarkTest {

	@Autowired
	JooqCountryService jooqCountryService;

	@Autowired
	CountryService myBatisCountryService;

	Logger logger = Logger.getLogger(DwPosttradeBenchmarkApplication.class.getSimpleName());

	@Test
	@Order(1)
	void fetchAllCountryTestJOOQ() {
		logger.info("------- JOOQ:  fetch  country count test");
		int jooqSize = jooqCountryService.getCountryList().size();
		logger.info(String.format("-------  JOOQ: end fetch country count test   %d", jooqSize));
	}

	@Test
	@Order(2)
	void fetchAllCountryTestMyBatis() {
		logger.info("------- MYBATIS:  fetch  country count test");
		int mybatisSize = myBatisCountryService.getCountryList().size();
		logger.info(String.format("-------  MYBATIS: end fetch country count test   %d", mybatisSize));
	}

	@Test
	@Order(3)
	void fetchAllCityTestJOOQ() {
		logger.info("------- JOOQ:  fetch  city count test");
		int jooqSize = jooqCountryService.getCityList().size();
		logger.info(String.format("-------  JOOQ: end fetch city count test   %d", jooqSize));
	}

	@Test
	@Order(4)
	void fetchAllCityTestMyBatis() {
		logger.info("------- MYBATIS:  fetch  city count test");
		int mybatisSize = myBatisCountryService.getCityList().size();
		logger.info(String.format("-------  MYBATIS: end fetch city count test   %d", mybatisSize));
	}

	@Test
	@Order(5)
	void fetchAllCountryWithCitiesJOOQ() {
		logger.info("------- JOOQ: left join  test");
		int mybatisSize = jooqCountryService.getCountryWithCities().length;
		logger.info(String.format("-------  JOOQ: end left join test   %d", mybatisSize));
	}

	@Test
	@Order(6)
	void fetchAllCountryWithCitiesMyBatis() {
		logger.info("------- MYBATIS:  left join test");
		int mybatisSize = myBatisCountryService.getCountryWithCities().size();
		logger.info(String.format("-------  MYBATIS: end left join test   %d", mybatisSize));
	}

}
