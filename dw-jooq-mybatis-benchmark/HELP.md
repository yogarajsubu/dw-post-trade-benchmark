## Requirement

1. PostgreSQL up and running.

## Benchmark Test data

1. Database Dump : https://www.postgresql.org/ftp/projects/pgFoundry/dbsamples/world/dbsamples-0.1/ 

## Running Benchmark Test
1. mvn clean install
2. mvn test

## Generating Test Report 

1. mvn site


