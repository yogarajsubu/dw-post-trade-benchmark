package com.ionixx.jasperpermformance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;

@Service
public class ReportService {

    private final Logger mLOG = LogManager.getLogger(ReportService.class);

    @Autowired
    RestTemplateBuilder mRestTemplateBuilder;

    public Mono<List<String>> generateReport(Supplier<String> reportGenId) {

        mLOG.info("Generating Report For ID = {} ", reportGenId.get());

        var requestBody = """
                  {
                    "reportUnitUri": "/reports/analysis/wiki_doc_reports",
                    "outputFormat": "pdf",
                    "async" : "true",
                    "ignorePagination": "true"
                }""";

        var header = new HttpHeaders();
        header.add("Authorization", "Basic amFzcGVyYWRtaW46dlVUUjBsQnZnNHJk");
        header.add("content-type", "application/json");

        var requestEnitity = new HttpEntity<String>(requestBody, header);

        return Mono.create(sink -> {

            try {
                var response = this.mRestTemplateBuilder.build().exchange(
                        "http://10.175.1.9:80/jasperserver/rest_v2/reportExecutions", HttpMethod.POST, requestEnitity,
                        String.class);

                mLOG.info("Response Body {}", response.getBody());
                mLOG.info("Response Headers Cookies {}", response.getHeaders().get("Set-Cookie").get(0));

                var responseObject = new JSONObject(response.getBody());
                // var exports = responseObject.getJSONArray("exports");

                // var exportObject = exports.getJSONObject(0);

                var responseList = new ArrayList<String>();

                responseList.add(responseObject.getString("requestId"));
                responseList.add(response.getHeaders().get("Set-Cookie").get(0));

                sink.success(responseList);
            } catch (Exception e) {
                sink.error(e);
            }

        });

    }

    // schedule an api call for every 5 secs.
    private ScheduledFuture<String> getExecutionStatus(String requestId, String cookie) throws JSONException {

        var header = new HttpHeaders();

        header.add("content-type", "application/json");
        header.add("accept", "application/status+json");
        // without setting cookie we will get 404 execption , we need to use the same
        // session
        header.add("Cookie", cookie);

        // check status every 5 second
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

        return service.schedule(new Callable<String>() {

            @Override
            public String call() throws Exception {

                var requestEnitity = new HttpEntity<String>(null, header);

                var response = mRestTemplateBuilder.build()
                        .exchange(
                                String.format("%s/%s/status",
                                        "http://10.175.1.9:80/jasperserver/rest_v2/reportExecutions", requestId),
                                HttpMethod.GET, requestEnitity, String.class);

                mLOG.info("Response Body {}", response.getBody());

                var responseObject = new JSONObject(response.getBody());

                var status = responseObject.getString("value");
                mLOG.info("Execution Status for Report ID = {} ", status);

                return status;
            }

        }, 5, TimeUnit.SECONDS);
    }

    // pool status of execution
    private void poolExecutionStatus(MonoSink<String> sink, Supplier<String> requestId, Supplier<String> cookies) {
        try {
            mLOG.info("\n \n Pooling Execution Status for Report ID = {}  ", requestId.get());

            ScheduledFuture<String> exeScheduledFuture = this.getExecutionStatus(requestId.get(), cookies.get());

            var completableFuture = CompletableFuture.supplyAsync(() -> {

                try {
                    return exeScheduledFuture.get();
                } catch (Exception e) {
                    return null;
                }
            });

            completableFuture.whenComplete((status, error) -> {

                if (error != null) {
                    sink.error(error);
                    return;
                }

                if (status == null) {
                    sink.error(new Exception("Report Generation Failed"));
                    return;
                }

                if (status.equals("ready")) {
                    sink.success(requestId.get());
                    return;
                }

                if (status.equals("failed")) {
                    sink.error(new Exception("Report Generation Failed"));
                    return;
                }

                this.poolExecutionStatus(sink, requestId, cookies);
            });

        } catch (Exception e) {
            sink.error(e);
        }
    }

    public Mono<String> poolExecutionStatus(Supplier<String> requestId, Supplier<String> cookies) {

        return Mono.create(sink -> {
            sink.onRequest(consumerID -> {
                this.poolExecutionStatus(sink, requestId, cookies);
            });
        });
    }

}
