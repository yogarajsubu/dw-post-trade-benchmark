package com.ionixx.jasperpermformance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JasperpermformanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasperpermformanceApplication.class, args);
	}

}
