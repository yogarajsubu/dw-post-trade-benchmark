package com.ionixx.jasperpermformance.benchmark;

import java.util.concurrent.TimeUnit;

import com.ionixx.jasperpermformance.service.ReportService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;

@Component
public class JasperBenchMark {

    private final Logger mLOG = LogManager.getLogger(JasperBenchMark.class);

    Timer timer;

    @Autowired
    ReportService mJasperReportService;

    public JasperBenchMark(MeterRegistry meterRegistry) {

        this.timer = Timer.builder("jasper_report_execution_timer").tag("benchmark", "jasper_report")
                .register(meterRegistry);
    }

    @EventListener(value = ApplicationReadyEvent.class)
    public void startBenchMark() {
        int executionCount = 5;
        int warmpUpcount = 0;
        this.warmup(warmpUpcount);
        this.execute(executionCount);
    }

    private void warmup(Integer loopCount) {
        mLOG.info("\n");
        mLOG.info("========= Starting Warmup =======");
        for (var i = 0; i < loopCount; i++) {
            var generationId = String.format("WARMUP-IDX-%d", loopCount);
            this.generateReport(generationId);
        }
        mLOG.info(" ===== Warmup Completed ===== ");
        mLOG.info("\n");
        mLOG.info("\n");
    }

    private void execute(Integer loopCount) {

        mLOG.info("\n");
        mLOG.info("======== Starting Execution ======== ");
        for (var i = 0; i < loopCount; i++) {

            var generationId = String.format("WARMUP-IDX-%d", i);
            this.generateReport(generationId);

        }
        mLOG.info("Execution Completed");
        mLOG.info("\n");
    }

    private void generateReport(String generateReportId) {

        var currentTimeStamp = System.nanoTime();
        this.mJasperReportService.generateReport(() -> generateReportId).doOnSuccess(reportResponse -> {
            var message = String.format("Report  Generation Request Sucessful  for ID = %s  REPORT ID = %s ",
                    generateReportId, reportResponse.get(0));
            this.mLOG.info(message);
        }).doOnError(Exception.class, e -> {

            var message = String.format("Report  Generation Request Failed  for ID = %s  REPORT Error = %s ",
                    generateReportId, e.getMessage());
            this.timer.record(System.nanoTime() - currentTimeStamp, TimeUnit.NANOSECONDS);
            this.mLOG.info(message);

        }).flatMap(reportResponse -> {

            return this.mJasperReportService
                    .poolExecutionStatus(() -> reportResponse.get(0), () -> reportResponse.get(1))
                    .doOnError(Exception.class, (e) -> {
                        mLOG.info(String.format("Report  Generation Failed  for ID = %s  REPORT Error = %s ",
                                generateReportId, e.getMessage()));
                        this.timer.record(System.nanoTime() - currentTimeStamp, TimeUnit.NANOSECONDS);
                    });

        }).subscribe(completionId -> {
            this.timer.record(System.nanoTime() - currentTimeStamp, TimeUnit.NANOSECONDS);
            mLOG.info(String.format("Report  Generation Sucess  for ID = %s  REPORT ID = %s ", generateReportId,
                    completionId));
        });

    }

}
